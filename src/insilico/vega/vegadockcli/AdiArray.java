/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insilico.vega.vegadockcli;

import insilico.core.ad.item.ADIndex;
import insilico.core.ad.item.iADIndex;
import insilico.core.exception.GenericFailureException;
import insilico.core.model.InsilicoModelOutput;
import insilico.core.model.runner.InsilicoModelRunnerByMolecule;
import insilico.core.model.runner.InsilicoModelWrapper;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.model.utils.ModelsList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Cosimo-DEV
 */
public class AdiArray {

    /**
     *
     */
    public double AdiValue;

    /**
     *
     */
    public String AdiName;

    /**
     *
     * @param AdiName the name of the ADI component
     * @param AdiValue the value of the ADI component
     */
    public AdiArray(String AdiName, double AdiValue) {
        this.AdiName = AdiName;
        this.AdiValue = AdiValue;
    }

    /**
     *
     * @param model
     * @param Smiles
     * @return a list of The ADI components for the selected value with the current value for the selected smiles
     * @throws GenericFailureException
     */
    public static List<AdiArray> getADI(String model, String Smiles) throws GenericFailureException {
        List<AdiArray> AdiList = new ArrayList();
        InsilicoModelRunnerByMolecule Runner = new InsilicoModelRunnerByMolecule();
        Runner.AddModel(ModelsList.getModel(model));
        Runner.Run(SmilesMolecule.Convert(Smiles));
        InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
        InsilicoModelOutput R = (InsilicoModelOutput) mw.getResult().get(0);
        //Set ADI to 0 if there are some errors
        if (!R.getErrMessage().isEmpty()) {
           AdiList.add(new AdiArray("Applicability Domain Index", 0));
            return AdiList;
        }
        
        AdiList.add(new AdiArray("Applicability Domain Index", R.getADI().GetIndexValue()));
        
        for (iADIndex i : R.getADIndex()) {

            AdiList.add(new AdiArray("ADI component: "+ i.GetIndexName(), i.GetIndexValue()));
        }

        return AdiList;
    }

}
