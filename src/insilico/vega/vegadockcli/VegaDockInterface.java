/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insilico.vega.vegadockcli;

import insilico.core.exception.InitFailureException;
import insilico.core.model.InsilicoModelOutput;
import insilico.core.model.runner.InsilicoModelRunnerByMolecule;
import insilico.core.model.runner.InsilicoModelWrapper;
import insilico.core.model.trainingset.iTrainingSet;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.similarity.SimilarMolecule;
import insilico.core.tools.logger.InsilicoLogger;
import insilico.model.utils.ModelsList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.apache.commons.cli.Options;

/**
 *
 * @author Cosimo-DEV
 */
public class VegaDockInterface {

    private final String model;
    private final String output;
    private final String Smiles;

    /**
     *
     */
    public VegaDockInterface() {

        this.model = new String();
        this.output = new String();
        this.Smiles = new String();

    }

    /**
     *
     * @param model
     * @param output
     * @param Smiles
     * @return
     */
    public static String getValues(String model, String output, String Smiles) {
        String cur_out = "error";
        InsilicoMolecule m = new InsilicoMolecule();
//			Runner.Run(m);
//			InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
//			InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
        // Add define outcome
        // for optimization, if options don't require prediction, run before applying the runner
        // these options can be run without smiles
        try {
            if (output.equalsIgnoreCase("unit")) {

                if (ModelsList.getModel(model).getInfo().getUnits() != null && !ModelsList.getModel(model).getInfo().getUnits().isEmpty()) {
                    cur_out = ModelsList.getModel(model).getInfo().getUnits();
//                        if (mw.getModel().GetTrainingSet().hasUnits()){
//                        cur_out = mw.getModel().GetTrainingSet().getUnits(); 
                } else {
                    cur_out = "no unit";
                }
                return cur_out;

            }
            if (output.equalsIgnoreCase("name")) {

                cur_out = ModelsList.getModel(model).getInfo().getName();

                return cur_out;

            }

            if (output.equalsIgnoreCase("QMRFURL")) {

                cur_out = ModelsList.getModel(model).getInfo().getQMRFLink();

                return cur_out;

            }
            if (output.equalsIgnoreCase("description_long")) {

                cur_out = ModelsList.getModel(model).getInfo().getDescriptionLong();

                return cur_out;

            }
            if (output.equalsIgnoreCase("description")) {

                cur_out = ModelsList.getModel(model).getInfo().getDescription();

                return cur_out;

            }
            if (output.equalsIgnoreCase("guide")) {

                cur_out = ModelsList.getModel(model).getInfo().getGuideURL();;

                return cur_out;

            }

            //the previous options can run without s SMILES, the remaining are related to the predictions.
            //it's required a SMILES
            try {
                InsilicoLogger.InitLogger();
                m = SmilesMolecule.Convert(Smiles);
            } catch (InitFailureException e) {
                return cur_out;
            }
            InsilicoModelRunnerByMolecule Runner = new InsilicoModelRunnerByMolecule();
            Runner.AddModel(ModelsList.getModel(model));
            Runner.Run(m);
            InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
            InsilicoModelOutput R = (InsilicoModelOutput) mw.getResult().get(0);
           if (!R.getErrMessage().isEmpty()) {
            cur_out = R.getErrMessage();

            return cur_out;
        }

            if (output.equalsIgnoreCase("prediction")) {
                if (mw.getModel().getInfo().hasClassValues()) {
                    cur_out = R.getResults()[0];

                } else {
                    cur_out = String.format("%.2f", R.getMainResultValue());
                }
                return cur_out;
            }

            if (output.equalsIgnoreCase("assessment")) {
                cur_out = R.getAssessment();

                return cur_out;
            }

            if (output.equalsIgnoreCase("assessment_verbose")) {
                cur_out = R.getAssessmentVerbose();

                return cur_out;
            }
            if (output.equalsIgnoreCase("error")) {
                cur_out = R.getErrMessage();

                return cur_out;
            }
            if (output.equalsIgnoreCase("ADI")) {
                cur_out = R.getADI().GetIndexValueFormatted();

                return cur_out;
            }
            if (output.equalsIgnoreCase("Experimental")) {

                cur_out = R.getExperimentalFormatted();

                return cur_out;
            }
            if (output.equalsIgnoreCase("Similar_molecules_index")) {

                ArrayList<SimilarMolecule> Sim_mols = R.getSimilarMolecules();
                if (!Sim_mols.isEmpty()) {
                    StringBuilder sb = new StringBuilder();
                    for (SimilarMolecule Sim_mol : Sim_mols) {

                        sb.append(Sim_mol.getIndex()).append(";");
                    }

                    cur_out = sb.deleteCharAt(sb.length() - 1).toString();
                }

                return cur_out;
            }
            if (output.equalsIgnoreCase("Similar_molecules_Smiles")) {
                iTrainingSet TrainSet = mw.getModel().GetTrainingSet();

                ArrayList<SimilarMolecule> Sim_mols = R.getSimilarMolecules();
                if (!Sim_mols.isEmpty()) {
                    StringBuilder sb = new StringBuilder();

                    for (SimilarMolecule Sim_mol : Sim_mols) {
                        int cur_ind = (int) Sim_mol.getIndex();
                        sb.append(TrainSet.getSMILES(cur_ind)).append(";");
                    }
                    cur_out = sb.deleteCharAt(sb.length() - 1).toString();
                }

                return cur_out;
            }

        } catch (Exception e) {
            cur_out = "error";
        }

        return cur_out;

    }

}
