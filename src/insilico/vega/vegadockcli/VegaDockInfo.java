/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insilico.vega.vegadockcli;

import insilico.core.exception.InitFailureException;
import insilico.core.model.InsilicoModelOutput;
import insilico.core.model.runner.InsilicoModelRunnerByMolecule;
import insilico.core.model.runner.InsilicoModelWrapper;
import insilico.core.model.trainingset.iTrainingSet;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.similarity.SimilarMolecule;
import insilico.core.tools.logger.InsilicoLogger;
import insilico.model.utils.ModelsList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.apache.commons.cli.Options;

/**
 *
 * @author Cosimo-DEV
 */
public class VegaDockInfo {

    private final String model;

    /**
     *
     */
    public static String Unit;

    /**
     *
     */
    public static String Name;

    /**
     *
     */
    public static String GuideURL;

    /**
     *
     */
    public static String QMRFLink;

    /**
     *
     */
    public static String DescriptionLong;

    /**
     *
     */
    public static String Description;

    /**
     *
     */
    public static String key;

    /**
     *
     */
    public static HashMap<Double, String> ClassValues;

    /**
     *
     */
    public static ArrayList<String[]> TrainingSet;

    /**
     *
     */
    public static ArrayList<String[]> TestSet;

    /**
     *
     */
    public VegaDockInfo() {

        this.model = new String();
        this.Unit = new String();
        this.Name = new String();
        this.GuideURL = new String();
        this.QMRFLink = new String();
        this.DescriptionLong = new String();
        this.Description = new String();
        this.key = new String();
        this.ClassValues = new HashMap<Double, String>();
        this.TrainingSet = new ArrayList<String[]>();
        this.TestSet = new ArrayList<String[]>();
        

    }

    /**
     *
     * @param model
     */
    public  void run(String model) {
        try {
            if (ModelsList.getModel(model).getInfo().getUnits() != null && !ModelsList.getModel(model).getInfo().getUnits().isEmpty()) {
                this.Unit = ModelsList.getModel(model).getInfo().getUnits();
            } else {
                this.Unit = "no unit";
            }
            this.Name = ModelsList.getModel(model).getInfo().getName();
            this.GuideURL = ModelsList.getModel(model).getInfo().getGuideURL();
            this.QMRFLink = ModelsList.getModel(model).getInfo().getQMRFLink();
            this.DescriptionLong =  ModelsList.getModel(model).getInfo().getDescriptionLong();
            this.Description =  ModelsList.getModel(model).getInfo().getDescription();
            this.key =  ModelsList.getModel(model).getInfo().getKey();
            this.ClassValues =  ModelsList.getModel(model).getInfo().getClassValues();

            
        } catch (Exception e) {
            
        }
    }

    /**
     *
     * @return
     */
    public String getUnit() { return this.Unit;}

    /**
     *
     * @return
     */
    public String getName() { return this.Name;}

    /**
     *
     * @return
     */
    public  String getGuideURL() {return this.GuideURL;}

    /**
     *
     * @return
     */
    public  String getQMRFLink() {return this.QMRFLink;}

    /**
     *
     * @return
     */
    public  String getDescriptionLong() {return this.DescriptionLong;}

    /**
     *
     * @return
     */
    public  String getDescription() {return this.Description;}
    
    /**
     *
     * @return
     */
    public  String getkey() {return this.key;}
    
    /**
     *
     * @return
     */
    public  HashMap<Double, String> getClassValues() {return this.ClassValues;}
    
    /**
     *
     * @return
     */
    public  ArrayList<String[]> getTrainingSet() {return this.TrainingSet;}
     
    /**
     *
     * @return
     */
    public  ArrayList<String[]>getTestSet() {return this.TestSet;}

    

}
