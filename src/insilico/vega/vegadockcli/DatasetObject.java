/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package insilico.vega.vegadockcli;

import insilico.core.exception.GenericFailureException;
import insilico.core.model.trainingset.iTrainingSet;
import insilico.model.utils.ModelsList;

/**
 *
 * @author kosmu
 */
public class DatasetObject {
    
    private final String model;

    /**
     *
     */
    public int nMol;

    /**
     *
     */
    public String[] Smiles;

    /**
     *
     */
    public  int[] CAS;

    /**
     *
     */
    public  double[] Experimental;

    /**
     *
     */
    public DatasetObject() {
        this.model = new String();
        this.nMol = 0;
        this.Smiles = null;
        this.CAS = null;
        this.Experimental = null;

    }

    /**
     *
     * @param model
     * @param Set
     */
    public void run(String model,String Set) {
         try{
             Short Status= -1;
          iTrainingSet trainingset=  ModelsList.getModel(model).GetTrainingSet(); 
         if (Set=="Training"){
         this.nMol =    trainingset.getMoleculesTrainSize();
         Status = 1;
             }
         if (Set=="Test"){
         this.nMol =    trainingset.getMoleculesTestSize();
         Status = 2;
             } 
         this.Smiles= new String[this.nMol];
         this.CAS=  new int[this.nMol];
         this.Experimental=  new double[this.nMol];
         int idx=0;    
         for (int i = 0 ;i<nMol; i++){
         if (trainingset.getMoleculeSet(i)==Status)   {
         this.Smiles[idx] = trainingset.getSMILES(i);
         this.CAS[idx] = (int)Integer.parseInt(trainingset.getCAS(i).replace("-", ""));
         this.Experimental[idx] = trainingset.getExperimentalValue(i);
         idx++;
         }
         }
         } catch(Exception e){
        this.nMol = 1;
        this.Smiles[0] = "";
        this.CAS[0] = 0;
        this.Experimental[0] = -999.0; 
                 }
         
     }
    
    /**
     *
     * @return return an array of SMILES values of Set
     */
    public  String[] getSmilesSet() {return this.Smiles;}
    
    /**
     *
     * @return an array of CAS numbers (parsed as int after removing the "-") of Set
     */
    public  int[] getCASSet() {return this.CAS;}
    
    /**
     *
     * @return an array of experimental values of Set
     */
    public  double[] getExperimentalSet() {return this.Experimental;}
    
    /**
     *
     * @return
     */
    public  int getnMol() {
        return this.nMol;}

    /**
     *
     * @param model The model to evaluate
     * @return Number of Molecules in Test
     * @throws insilico.core.exception.GenericFailureException
     */
    public  int getnMolTest(String model) throws GenericFailureException {
         return    ModelsList.getModel(model).GetTrainingSet().getMoleculesTestSize();
         }
    
        /**
     *
     * @param model The model to evaluate
     * @return Number of Molecules in Train
     * @throws insilico.core.exception.GenericFailureException
     */
            public  int getnMolTrain(String model) throws GenericFailureException {
         return   ModelsList.getModel(model).GetTrainingSet().getMoleculesTestSize();
         }
    
}
