/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insilico.vega.vegadockcli;

import insilico.core.ad.item.iADIndex;
import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InitFailureException;
import insilico.core.model.InsilicoModel;
import insilico.core.model.InsilicoModelOutput;
import insilico.core.model.iInsilicoModel;
import insilico.core.model.runner.InsilicoModelRunnerByMolecule;
import insilico.core.model.runner.InsilicoModelWrapper;
import insilico.core.model.trainingset.iTrainingSet;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.similarity.SimilarMolecule;
import insilico.core.tools.ModelUtilities;
import insilico.core.tools.logger.InsilicoLogger;
import insilico.core.version.InsilicoInfo;
import insilico.model.utils.ModelsList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import org.apache.commons.cli.*;

/**
 *
 * @author Cosimo-DEV
 */
public class VegaDockCLI {

    private final Options options;

    /**
     *
     */
    public VegaDockCLI() {

        this.options = new Options();
        this.options.addOption("s", "SMILES", true, "Input SMILES").addOption("m", "model", true, "Model")
                .addOption("o", "output", true, "Output type (Prediction, Unit, ADI index...")
                .addOption("h", "help", false, "Print help file").addOption("l", "modellist", false, "list of model files")
                .addOption("k", "outputlist", false, "list of Possible outputs");

    }

    private void PrintModelsList() {
        System.out.println("List of tags for currently available VEGA models (-m option) :");
        System.out.println();
        for (String s : ModelsList.getModelsTags()) {
            System.out.print("[" + s + "] ");
            try {
                //suppress Toxtree warnings
                PrintStream original = System.out;
                System.setOut(new PrintStream(new OutputStream() {
                    public void write(int b) {
                        //DO NOTHING
                    }
                }));
                String Description = ModelsList.getModel(s).getInfo().getDescription();
                System.setOut(original);

                System.out.println(Description + " ver."
                        + ModelsList.getModel(s).getInfo().getVersion());
            } catch (GenericFailureException ex) {
                System.out.println("[ERROR - unable to init model]");
            }
        }
    }

    private void PrintOutputList() {
        System.out.println("List of possible outputs (-o option):");
        System.out.println("N.B. options with * can be run without input");
        System.out.println();
        for (String s : OutputList.getOutputTags()) {
            System.out.print("[" + s + "] ");
            System.out.println(OutputList.getDescription(s));

        }

    }

    // Create Method To Print Help in case of errors
    private void PrintHelp() {
        HelpFormatter formatter = new HelpFormatter();
        System.out.println("Example: -s c1ccccc1 -o assessment -m fish_lc50");
        formatter.printHelp("java -jar VEGA-CLI.jar", options);
        System.out.println();
        PrintModelsList();
        System.out.println();
        PrintOutputList();

    }

    /**
     *
     * @param args
     * @return
     * @throws GenericFailureException
     * @throws InitFailureException
     */
    public String Launch(String[] args) throws GenericFailureException, InitFailureException {
        String cur_out = new String();
        CommandLine line;
        CommandLineParser parser = new GnuParser();
        InsilicoMolecule m = new InsilicoMolecule();

        try {
            line = parser.parse(this.options, args);
        } catch (ParseException e) {
            PrintHelp();
            throw new GenericFailureException("arguments can't be parsed");
        }

        // print help if no args are passed
        if (line.getOptions().length < 1) {
            PrintHelp();
            return cur_out;
        }

        String input = line.getOptionValue("s");
        String output = line.getOptionValue("o");
        String model = line.getOptionValue("m");

        // print Help
        if (line.hasOption("h")) {
            PrintHelp();
            return cur_out;
        }
        //ptinr model's list
        if (line.hasOption("l")) {
            PrintModelsList();
            return cur_out;
        }
        //print output list
        if (line.hasOption("k")) {
            PrintOutputList();
            return cur_out;
        }
        if (!ModelsList.ModelTagExists(model)) {
            System.out.println("Model doesn't exist or tag is not spelled correctly");
            System.out.println();
            PrintHelp();
            return cur_out;
        }
        if (!OutputList.tagExists(output)) {
            System.out.println("Selected output doesn't exist or tag is not spelled correctly");
            System.out.println();
            PrintHelp();
            return cur_out;
        }
			//

//			Runner.Run(m);
//			InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
//			InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
        // Add define outcome
        // for optimization, if options don't require prediction, run before applying the runner
        // these options can be run without smiles
        if (output.equalsIgnoreCase("unit")) {

            if (ModelsList.getModel(model).getInfo().getUnits() != null && !ModelsList.getModel(model).getInfo().getUnits().isEmpty()) {
                cur_out = ModelsList.getModel(model).getInfo().getUnits();
//                        if (mw.getModel().GetTrainingSet().hasUnits()){
//                        cur_out = mw.getModel().GetTrainingSet().getUnits(); 
            } else {
                cur_out = "no unit";
            }
            System.out.println(cur_out);
            return cur_out;

        }
        if (output.equalsIgnoreCase("name")) {

            cur_out = ModelsList.getModel(model).getInfo().getName();
            System.out.println(cur_out);
            return cur_out;

        }

        if (output.equalsIgnoreCase("QMRFURL")) {

            cur_out = ModelsList.getModel(model).getInfo().getQMRFLink();
            System.out.println(cur_out);
            return cur_out;

        }
        if (output.equalsIgnoreCase("description_long")) {

            cur_out = ModelsList.getModel(model).getInfo().getDescriptionLong();
            System.out.println(cur_out);
            return cur_out;

        }
        if (output.equalsIgnoreCase("description")) {

            cur_out = ModelsList.getModel(model).getInfo().getDescription();
            System.out.println(cur_out);
            return cur_out;

        }
        if (output.equalsIgnoreCase("guide")) {

            cur_out = ModelsList.getModel(model).getInfo().getGuideURL();;
            System.out.println(cur_out);
            return cur_out;

        }

            //the previous options can run without s SMILES, the remaining are related to the predictions.
        //it's required a SMILES
        if (!line.hasOption("s")) {
            System.out.println("please provide a SMILES");
            return cur_out;
        }

        try {
            InsilicoLogger.InitLogger();
            m = SmilesMolecule.Convert(input);
        } catch (InitFailureException e) {
            return cur_out;
        }
        InsilicoModelRunnerByMolecule Runner = new InsilicoModelRunnerByMolecule();
        Runner.AddModel(ModelsList.getModel(model));
        Runner.Run(m);
        InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
        InsilicoModelOutput R = (InsilicoModelOutput) mw.getResult().get(0);
       
        if (!R.getErrMessage().isEmpty()) {
            cur_out = R.getErrMessage();
            System.out.println(cur_out);

            return cur_out;
        }

        if (output.equalsIgnoreCase("prediction")) {
            if (mw.getModel().getInfo().hasClassValues()) {
                cur_out = R.getResults()[0];
            } else {
                cur_out = String.format("%.2f", R.getMainResultValue());
            }
            System.out.println(cur_out);
            return cur_out;
        }
        if (output.equalsIgnoreCase("assessment")) {
            cur_out = R.getAssessment();
            System.out.println(cur_out);

            return cur_out;
        }

        if (output.equalsIgnoreCase("assessment_verbose")) {
            cur_out = R.getAssessmentVerbose();
            System.out.println(cur_out);

            return cur_out;
        }
        if (output.equalsIgnoreCase("error")) {
            cur_out = R.getErrMessage();
            System.out.println(cur_out);

            return cur_out;
        }
        if (output.equalsIgnoreCase("ADI")) {
            cur_out = R.getADI().GetIndexValueFormatted();
            System.out.println(cur_out);

            return cur_out;
        }
        if (output.equalsIgnoreCase("Experimental")) {

            cur_out = R.getExperimentalFormatted();

            System.out.println(cur_out);

            return cur_out;
        }
        if (output.equalsIgnoreCase("Similar_molecules_index")) {

            ArrayList<SimilarMolecule> Sim_mols = R.getSimilarMolecules();
            if (!Sim_mols.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (SimilarMolecule Sim_mol : Sim_mols) {

                    sb.append(Sim_mol.getIndex()).append(";");
                }

                cur_out = sb.deleteCharAt(sb.length() - 1).toString();
            }
            System.out.println(cur_out);

            return cur_out;
        }
        if (output.equalsIgnoreCase("Similar_molecules_Smiles")) {
            iTrainingSet TrainSet = mw.getModel().GetTrainingSet();

            ArrayList<SimilarMolecule> Sim_mols = R.getSimilarMolecules();
            if (!Sim_mols.isEmpty()) {
                StringBuilder sb = new StringBuilder();

                for (SimilarMolecule Sim_mol : Sim_mols) {
                    int cur_ind = (int) Sim_mol.getIndex();
                    sb.append(TrainSet.getSMILES(cur_ind)).append(";");
                }
                cur_out = sb.deleteCharAt(sb.length() - 1).toString();
            }
            System.out.println(cur_out);

            return cur_out;
        }

        return cur_out;
    }

}
