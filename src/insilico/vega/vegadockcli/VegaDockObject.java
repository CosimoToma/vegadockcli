/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insilico.vega.vegadockcli;

import insilico.core.exception.InitFailureException;
import insilico.core.model.InsilicoModel;
import insilico.core.model.InsilicoModelConsensusOutput;
import insilico.core.model.InsilicoModelOutput;
import insilico.core.model.runner.InsilicoModelConsensusWrapper;
import insilico.core.model.runner.InsilicoModelRunnerByMolecule;
import insilico.core.model.runner.InsilicoModelWrapper;
import insilico.core.model.trainingset.iTrainingSet;
import insilico.core.molecule.InsilicoMolecule;
import insilico.core.molecule.conversion.SmilesMolecule;
import insilico.core.similarity.SimilarMolecule;
import insilico.core.tools.logger.InsilicoLogger;
import insilico.model.utils.ModelsList;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 *
 * @author Cosimo-DEV
 */
public class VegaDockObject {

    private final String model;
    private final String Smiles;

    /**
     *
     */
    public static String prediction;

    /**
     *
     */
    public static String assessment;

    /**
     *
     */
    public static String assessment_verbose;

    /**
     *
     */
    public static String Experimental;

    /**
     *
     */
    public static String error;

    /**
     *
     */
    public static String ADI;

    /**
     *
     */
    public static String Similar_molecules_index;

    /**
     *
     */
    public static String Similar_molecules_smiles;

    /**
     *
     */
    public VegaDockObject() {

        this.model = new String();
        this.Smiles = new String();
        this.prediction = new String();
        this.assessment = new String();
        this.assessment_verbose = new String();
        this.Experimental = new String();
        this.error = new String();
        this.ADI = new String();
        this.Similar_molecules_index = new String();
        this.Similar_molecules_smiles = new String();

    }

    /**
     *
     * @param model
     * @param Smiles
     */
    public void run(String model, String Smiles) {
        InsilicoMolecule m = new InsilicoMolecule();
//			Runner.Run(m);
//			InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
//			InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
        try {
            InsilicoLogger.InitLogger();
            m = SmilesMolecule.Convert(Smiles);
        } catch (InitFailureException e) {
            //TODO att error thar can be passed
            this.error = "Error Converting SMILES";
            return;
        }
        try {
            InsilicoModelRunnerByMolecule Runner = new InsilicoModelRunnerByMolecule();
//            if (Pattern.matches(".*_cons$", model)){
//                ArrayList<InsilicoModel> RequiredModels= ModelsList.getModelconsensus(model).GetRequiredModels();
//                for ( InsilicoModel RequiredModel : RequiredModels){
//                    Runner.AddModel(RequiredModel);      
//                }
//               Runner.AddModel(ModelsList.getModelconsensus(model));
//              Runner.Run(m);
//              InsilicoModelConsensusWrapper mw = Runner.GetModelConsensusWrappers().get(0);
//            InsilicoModelConsensusOutput R = (InsilicoModelConsensusOutput) mw.getResult().get(0); 
//                       //handle the errors, don,t retrieve anything if there is some error
//          if (!R.getErrMessage().isEmpty()) {
//            this.error = R.getErrMessage();
//            return;
//        }
//
//            if (mw.getModel().getInfo().hasClassValues()) {
//                this.prediction = R.getResults()[0];
//
//            } else {
//                this.prediction = String.format("%.2f", R.getMainResultValue());
//            }
//
//            this.assessment = R.getAssessment();
//            this.assessment_verbose = R.getAssessmentVerbose();
//            this.error = R.getErrMessage();
//            //not applicable for Consensus
////            this.Experimental = R.
////                    getExperimentalFormatted();
////            this.ADI = R.getADI().GetIndexValueFormatted();
////            iTrainingSet TrainSet = mw.getModel().GetTrainingSet();
////
////            ArrayList<SimilarMolecule> Sim_mols = R.getSimilarMolecules();
////            if (!Sim_mols.isEmpty()) {
////                StringBuilder simidxb = new StringBuilder();
////                StringBuilder simsmilesb = new StringBuilder();
////                for (SimilarMolecule Sim_mol : Sim_mols) {
////                    int cur_ind = (int) Sim_mol.getIndex();
////                    simidxb.append(cur_ind).append("\n");
////                    simsmilesb.append(TrainSet.getSMILES(cur_ind)).append("\n");
////
////                }
////
////                this.Similar_molecules_index = simidxb.deleteCharAt(simidxb.length() - 1).toString();
////                this.Similar_molecules_smiles = simsmilesb.deleteCharAt(simsmilesb.length() - 1).toString();
////            }
//            }else{
            Runner.AddModel(ModelsList.getModel(model));
            Runner.Run(m);
            InsilicoModelWrapper mw = Runner.GetModelWrappers().get(0);
            InsilicoModelOutput R = (InsilicoModelOutput) mw.getResult().get(0);
                         
            
           //handle the errors, don,t retrieve anything if there is some error
          if (!R.getErrMessage().isEmpty()) {
            this.error = R.getErrMessage();
            return;
        }

            if (mw.getModel().getInfo().hasClassValues()) {
                this.prediction = R.getResults()[0];

            } else {
                this.prediction = String.format(Locale.US,"%.2f", R.getMainResultValue());
            }

            this.assessment = R.getAssessment();
            this.assessment_verbose = R.getAssessmentVerbose();
            this.error = R.getErrMessage();
            this.Experimental = R.getExperimentalFormatted();
            this.ADI = String.format(Locale.US,"%.2f", R.getADI().GetIndexValue());
            iTrainingSet TrainSet = mw.getModel().GetTrainingSet();

            ArrayList<SimilarMolecule> Sim_mols = R.getSimilarMolecules();
            if (!Sim_mols.isEmpty()) {
                StringBuilder simidxb = new StringBuilder();
                StringBuilder simsmilesb = new StringBuilder();
                for (SimilarMolecule Sim_mol : Sim_mols) {
                    int cur_ind = (int) Sim_mol.getIndex();
                    simidxb.append(cur_ind).append("\n");
                    simsmilesb.append(TrainSet.getSMILES(cur_ind)).append("\n");

                }

                this.Similar_molecules_index = simidxb.deleteCharAt(simidxb.length() - 1).toString();
                this.Similar_molecules_smiles = simsmilesb.deleteCharAt(simsmilesb.length() - 1).toString();
            }
//            }

        } catch (Exception e) {

        }

    }

}
