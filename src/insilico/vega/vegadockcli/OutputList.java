/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insilico.vega.vegadockcli;

import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InitFailureException;
import insilico.core.model.InsilicoModel;
import insilico.model.algae.combaseClass.ismAlgaeCombaseClass;
import insilico.model.algae.combaseEC50.ismAlgaeCombaseEC50;
import insilico.model.algae.ec50.ismAlgaeEC50;
import insilico.model.algae.noec.ismAlgaeNOEC;
import insilico.model.bcf.arnotgobas.ismBCFArnotGobas;
import insilico.model.bcf.caesar.ismBCFCaesar;
import insilico.model.bcf.knn.ismBCFKnn;
import insilico.model.bcf.meylan.ismBCFMeylan;
import insilico.model.bee.knn.ismBeeKnn;
import insilico.model.carcinogenicity.antares.ismCarcinogenicityAntares;
import insilico.model.carcinogenicity.bb.ismCarcinogenicityBB;
import insilico.model.carcinogenicity.caesar.ismCarcinogenicityCaesar;
import insilico.model.carcinogenicity.isscancgx.ismCarcinogenicityIsscanCgx;
import insilico.model.carcinogenicity.sficlassification.ismCarcinogenicitySFIClassification;
import insilico.model.carcinogenicity.sfiregression.ismCarcinogenicitySFIRegression;
import insilico.model.carcinogenicity.sfoclassification.ismCarcinogenicitySFOClassification;
import insilico.model.carcinogenicity.sforegression.ismCarcinogenicitySFORegression;
import insilico.model.daphnia.combase.ismDaphniaCombase;
import insilico.model.daphnia.demetra.ismDaphniaDemetra;
import insilico.model.daphnia.ec50.ismDaphniaEC50;
import insilico.model.daphnia.epa.ismDaphniaEPA;
import insilico.model.daphnia.noec.ismDaphniaNOEC;
import insilico.model.devtox.caesar.ismDevtoxCaesar;
import insilico.model.devtox.pg.ismDevToxPG;
import insilico.model.fathead.epa.ismFatheadEPA;
import insilico.model.fathead.knn.ismFatheadKnn;
import insilico.model.fish.combase.ismFishCombase;
import insilico.model.fish.irfmn.ismFishIRFMN;
import insilico.model.fish.knn.ismFishKnn;
import insilico.model.fish.lc50.ismFishLC50;
import insilico.model.fish.nic.ismFishNic;
import insilico.model.fish.noec.ismFishNOEC;
import insilico.model.guppy.knn.ismGuppyKnn;
import insilico.model.hepatotoxicity.irfmn.ismHepatotoxicityIrfmn;
import insilico.model.km.arnot.ismKmArnot;
import insilico.model.logp.alogp.ismLogPALogP;
import insilico.model.logp.meylan.ismLogPMeylan;
import insilico.model.logp.mlogp.ismLogPMLogP;
import insilico.model.mutagenicity.bb.ismMutagenicityBB;
import insilico.model.mutagenicity.caesar.ismMutagenicityCaesar;
import insilico.model.mutagenicity.knn.ismMutagenicityKnn;
import insilico.model.mutagenicity.sarpy.ismMutagenicitySarpy;
import insilico.model.persistence.sediment.irfmn.ismPersistenceSedimentIrfmn;
import insilico.model.persistence.sediment.quantitative_irfmn.ismPersistenceSedimentQuantitativeIrfmn;
import insilico.model.persistence.soil.irfmn.ismPersistenceSoilIrfmn;
import insilico.model.persistence.soil.quantitative_irfmn.ismPersistenceSoilQuantitativeIrfmn;
import insilico.model.persistence.water.irfmn.ismPersistenceWaterIrfmn;
import insilico.model.persistence.water.quantitative_irfmn.ismPersistenceWaterQuantitativeIrfmn;
import insilico.model.rba.cerapp.ismEstrogenBindingCerapp;
import insilico.model.rba.comparairfmn.ismAndrogenBindingComparaIRFMN;
import insilico.model.rba.irfmn.ismRbaIRFMN;
import insilico.model.readybio.irfmn.ismReadyBioIRFMN;
import insilico.model.skin.caesar.ismSkinCaesar;
import insilico.model.skin.irfmn.ismSkinIRFMN;
import insilico.model.sludge.combaseClass.ismSludgeCombaseClass;
import insilico.model.sludge.combaseEC50.ismSludgeCombaseEC50;
import insilico.model.watersolubility.irfmn.ismWaterSolubilityIRFMN;
import insilico.model.zebrafish.coral.ismZebrafishCoral;
import java.util.Arrays;

/**
 *
 * @author Cosimo-DEV
 */
public class OutputList {

    private static final String MODEL_NAME = "name";
    private static final String MODEL_UNIT = "unit";
    private static final String MODEL_PREDICTION = "prediction";
    private static final String MODEL_DESCRIPTION = "description";
    private static final String MODEL_DESCRIPTION_LONG = "description_long";
    private static final String MODEL_QMRFURL = "qmrfurl";
    private static final String MODEL_GUIDE = "GUIDE";
    private static final String MODEL_ASSESSMENT = "assessment";
    private static final String MODEL_ASSESSMENT_VERBOSE = "assessment_verbose";
    private static final String MODEL_ERROR = "error";
    private static final String MODEL_ADI = "adi";
    private static final String MODEL_Experimental = "experimental";
    private static final String MODEL_SIM_IDX = "similar_molecules_index";
    private static final String MODEL_SIM_SMILES = "similar_molecules_smiles";

    /**
     *
     */
    public static final String[] MODEL_TAGS = {"name", "unit", "description", "description_long", "qmrfurl", "guide", "prediction", "assessment", "assessment_verbose", "error", "adi",
        "experimental", "similar_molecules_index", "similar_molecules_smiles"};

    /**
     *
     * @return
     */
    public static String[] getOutputTags() {
        return MODEL_TAGS;
    }

    /**
     *
     * @param Tag
     * @return
     */
    public static boolean tagExists(String Tag) {
        return Arrays.asList(MODEL_TAGS).contains(Tag.toLowerCase());

    }

    /**
     *
     * @param Tag
     * @return
     */
    public static String getDescription(String Tag) {
        if (Tag.equalsIgnoreCase("name")) {
            return "name of the model*";
        }
        if (Tag.equalsIgnoreCase("unit")) {
            return "measure unit*";
        }
        if (Tag.equalsIgnoreCase("qmrfurl")) {
            return "Url of QMRF*";
        }
        if (Tag.equalsIgnoreCase("description")) {
            return "brief description of the model*";
        }
        if (Tag.equalsIgnoreCase("description_long")) {
            return "Detailed description of the model*";
        }
        if (Tag.equalsIgnoreCase("guide")) {
            return "URL of the guide fo the model in pdf format*";
        }
        if (Tag.equalsIgnoreCase("prediction")) {
            return "prediction round to second digit";
        }
        if (Tag.equalsIgnoreCase("assessment")) {
            return "Assessment (prediction/experimental with domain assessment)";
        }
        if (Tag.equalsIgnoreCase("assessment_verbose")) {
            return "Assessment with a brief comment about the result";
        }
        if (Tag.equalsIgnoreCase("error")) {
            return "error output";
        }
        if (Tag.equalsIgnoreCase("adi")) {
            return "Applicability Domain Index";
        }
        if (Tag.equalsIgnoreCase("qmrfurl")) {
            return "Applicability Domain index";
        }
        if (Tag.equalsIgnoreCase("experimental")) {
            return "Experimental (if present)";
        }
        if (Tag.equalsIgnoreCase("similar_molecules_index")) {
            return "Indexes of similar molecules from training set";
        }
        if (Tag.equalsIgnoreCase("similar_molecules_smiles")) {
            return "SMILES of similar molecules from training set";
        }

        return null;
    }

}
