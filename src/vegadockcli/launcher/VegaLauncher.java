/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vegadockcli.launcher;

import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InitFailureException;
import insilico.vega.vegadockcli.VegaDockCLI;

/**
 *
 * @author Cosimo-DEV
 */
public class VegaLauncher {

    /**
     *
     * @param args
     * @throws InitFailureException
     */
    public static void main(String[] args) throws InitFailureException {
		VegaDockCLI CLI = new VegaDockCLI();
		try {

			CLI.Launch(args);
                        
		} catch (GenericFailureException e) {
			System.out.println("errors occurred");
		}
//		System.out.println("Hello world");
	}

    
}
