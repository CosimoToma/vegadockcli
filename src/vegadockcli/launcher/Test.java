/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegadockcli.launcher;

import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InitFailureException;
import insilico.model.utils.ModelsList;
import insilico.vega.vegadockcli.VegaDockCLI;
import insilico.vega.vegadockcli.VegaDockInfo;
import insilico.vega.vegadockcli.VegaDockInterface;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 *
 * @author Cosimo-DEV
 */
public class Test {

    /**
     *
     * @param args
     * @throws InitFailureException
     */
    public static void main(String[] args) throws InitFailureException {

        try {
            VegaDockInfo vdi = new VegaDockInfo();
                    for (String s : ModelsList.getModelsTags()) {
            PrintStream original = System.out;
            System.setOut(new PrintStream(new OutputStream() {
                public void write(int b) {
                    //DO NOTHING
                }
            }));

            vdi.run(s);
             System.setOut(original);
            //System.out.println(VegaDockInterface.getValues("fish_lc50", "prediction", "c1ccccc1"));
            //System.out.println(s+ "\t" + vdi.getUnit()+ "\t"+ vdi.getDescription()+ "\t"+ vdi.getDescriptionLong()+ "\t" + vdi.getQMRFLink()+ "\t"+ vdi.getGuideURL());
             System.out.print(s+ "\t" + vdi.getUnit()+ "\t"+ vdi.getDescription()+ "\t"+ vdi.getDescriptionLong()+ "\t" + vdi.getQMRFLink()+ "\t"+ vdi.getGuideURL() + "\t" );
             int i=0;
             for (String value : vdi.getClassValues().values()){
             i++;
             System.out.print(value);
             if (i!= vdi.getClassValues().size()){ System.out.print(";");}
             }
             System.out.print("\n");
        }

        } catch (Exception e) {
            System.out.println("errors occurred");
        }
//		System.out.println("Hello world");
    }

}
