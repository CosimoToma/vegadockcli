/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vegadockcli.launcher;

import insilico.core.exception.GenericFailureException;
import insilico.core.exception.InitFailureException;
import insilico.vega.vegadockcli.DatasetObject;
import insilico.vega.vegadockcli.VegaDockCLI;
import insilico.vega.vegadockcli.VegaDockInfo;
import insilico.vega.vegadockcli.VegaDockInterface;
import insilico.vega.vegadockcli.VegaDockObject;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.regex.Pattern;

/**
 *
 * @author Cosimo-DEV
 */
public class TestTrainingretrieve {

    /**
     *
     * @param args
     * @throws InitFailureException
     */
    public static void main(String[] args) throws InitFailureException {

        try {
                       DatasetObject data = new DatasetObject();
//            if(Pattern.matches(".*_cons$", "muta_cons")){ System.out.println("yes");}
            data.run("daphnia_comb","Training");
            
//            System.out.println(vdi.error);

            //System.out.println(VegaDockInterface.getValues("fish_lc50", "prediction", "c1ccccc1"));
            //System.out.println(s+ "\t" + vdi.getUnit()+ "\t"+ vdi.getDescription()+ "\t"+ vdi.getDescriptionLong()+ "\t" + vdi.getQMRFLink()+ "\t"+ vdi.getGuideURL());
             System.out.print( data.getnMol()+ "\t"+data.getCASSet()[1]+ "\t"+ data.getExperimentalSet()[0]+ "\t"+data.getSmilesSet()[0]+"\n" );
           


        } catch (Exception e) {
            System.out.println("errors occurred");
        }
//		System.out.println("Hello world");
    }

}
